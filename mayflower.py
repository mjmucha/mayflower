__author__ = 'michalmucha'

import os, pkg_resources, webbrowser
import pandas, json
import numpy as np
from generate_random_color import generate_new_color


data_path = '/Users/michalmucha/Documents/mayflower/test_edgelist.csv'
result_path = '/Users/michalmucha/Documents/mayflower/'

def incidence_matrix(edgelist_dataframe):
    m = edgelist_dataframe.as_matrix()
    labels = np.unique(m[:,:2]).tolist()
    dimension = len(labels)
    label_dict = dict(zip(range(dimension),labels))
    inverse_labels = {v: k for k, v in label_dict.items()}
    matrix = np.zeros((dimension, dimension))
    for r in m:
        a, b, val = tuple(r)
        matrix[inverse_labels[a],inverse_labels[b]] = val

    return {'matrix': matrix.tolist(), 'label_dict': label_dict, 'maxvalue': m[:,2].max()}

def chord_diagram(incidence_matrix_data, output_folder='', open_browser_window=False):
    chord_html_template = pkg_resources.resource_string('mayflower', os.path.join('mayflower_templates', 'chord.html'))
    with open(output_folder + 'data.json', 'w', encoding='utf-8') as diagram_data_file:
        json.dump(incidence_matrix_data, diagram_data_file)
    with open(output_folder + 'chord.html', 'wb') as html_file:
        html_file.write(chord_html_template)
    if open_browser_window:
        webbrowser.open('chord.html')



def WebGL_globe(data):
    pass

def array_of_colors_hex(length):
    colors = []
    for i in range(length):
        colors.append(generate_new_color(colors, pastel_factor = 0.01))

    return ['#%02x%02x%02x' % tuple([255*x for x in c]) for c in colors]

if __name__ == '__main__':
    data_path = '/Users/michalmucha/Documents/mayflower/development/'
    df = pandas.read_csv(data_path + 'test_edgelist.csv', delimiter=';', header=None)
    data = incidence_matrix(df)
    chord_diagram(data, output_folder=data_path)
    # result = incidence_matrix(df)

    # print(chord_diagram(1))
    # result['colors'] = array_of_colors_hex(len(result['label_dict']))

    # json.dump(result, open(result_path + 'data.json', 'w'))